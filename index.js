/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AutocompleteScreen from './AutocompleteScreen';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AutocompleteScreen);
