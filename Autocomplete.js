import React, { Component } from 'react';
import { TextInput, FlatList, TouchableHighlight, Text, View} from 'react-native';

export default class Autocomplete extends Component {
    static defaultProps = {
        options: [],
        onChange: () => {},
    };

    state = {
        value: '',
        isFocused: false,
    };

    searchOptions = (options, str) => {
        return options.filter(item => item.toLowerCase().includes(str.toLowerCase()))
    };

    handleTextChange = (text) => {
        this.setState({
            value: text
        })
    };

    handleChoose = (item) => {
        this.setState({
            value: item,
            isFocused: false,
        }, () => {
            this.props.onChange(this.state.value);
        })
    };

    renderOption = ({ item }) => {
        return (
            <TouchableHighlight onPress={() => this.handleChoose(item)}>
                <Text>{item}</Text>
            </TouchableHighlight>
        )
    };

    render() {
        const { value, isFocused } = this.state;
        const { options } = this.props;
        const result = this.searchOptions(options, value);

        return (
            <View>
                <TextInput
                    value={value}
                    placeholder="Type here"
                    onChangeText={this.handleTextChange}
                    onFocus={() => this.setState({ isFocused: true })}
                />
                <View style={{ height: 150, padding: 5 }}>
                    {isFocused && (
                        <FlatList
                            keyExtractor={a => a}
                            data={result}
                            renderItem={this.renderOption}
                        />
                    )}
                </View>
            </View>
        )
    }
}
