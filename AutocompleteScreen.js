import React, { Component } from 'react';
import { Button, StyleSheet, Text, View} from 'react-native';
import Autocomplete from './Autocomplete';

export default class AutocompleteScreen extends Component {
  data = ['Antoine', 'Rémy', 'Florian', 'Maéva', 'Gaëtan', 'Jalil', 'Victor', 'Klervi', 'Florent'];

  state = {
    value: '',
    hasError: false
  };

  handleChange = (value) => {
    this.setState({
      value
    })
  };

  render() {
    const { value, hasError } = this.state;

    return (
      <View style={styles.container}>
        {hasError && <Text style={{ color: 'red' }}>Error!!!</Text>}
        <Text>Chosen: {value}</Text>
        <Autocomplete
          options={this.data}
          onChange={this.handleChange}
        />
        <Button title="Button" onPress={() => this.setState({ hasError: !value })}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
